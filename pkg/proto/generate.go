package proto

import (
	"bufio"
	"fmt"
	"io"
	"io/ioutil"
	"os"
	"strings"
)

func Generate(dir, output, protoPkg string) {
	files, err := ioutil.ReadDir(dir)
	if err != nil {
		panic(err)
	}

	cases := ""
	for _, file := range files {
		f, _ := os.Open(fmt.Sprintf("%s/%s", dir, file.Name()))
		if !strings.Contains(file.Name(), ".proto") {
			continue
		}
		buf := bufio.NewReader(f)
		for {
			line, err := buf.ReadString('\n')
			line = strings.TrimSpace(line)
			s := strings.Split(line, " ")
			if s[0] == "message" && s[2] == "{" {
				if strings.Contains(s[1], "c2s") {
					m := strings.Split(s[1], "_")
					cases += fmt.Sprintf("case %s:\n\t\treq := &pb.M_%s%s{}\n\t\tif err = proto.Unmarshal(buf, req); err != nil {\n\t\t\treturn\n\t\t}\n\t\treturn c.handle%s(req)\n\t", m[1], m[1], strings.ToUpper(m[2]), m[1])
				}
			}
			if err == io.EOF { //读取结束，会报EOF
				break
			}
		}
		_ = f.Close()
	}

	//	fmt.Println(cases)
	cont := fmt.Sprintf("package role\n\nimport (\n\t\"fmt\"\n\n\t\"google.golang.org/protobuf/proto\"\n\t\"google.golang.org/protobuf/reflect/protoreflect\"\n\t\"%s\"\n\n)\n\nfunc (c *Client) unpack(cmd uint16, buf []byte) (resp protoreflect.ProtoMessage, err error) {\n\tswitch cmd {\n\t%sdefault:\n\t\terr = fmt.Errorf(\"unknown cmd:%%d\", cmd)\n\t}\n\treturn\n}\n", protoPkg, cases)
	fmt.Println(ioutil.WriteFile(output, []byte(cont), 0666))
}
