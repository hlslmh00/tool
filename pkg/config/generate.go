package config

import (
	"fmt"
	"io/ioutil"
	"strings"

	"github.com/xuri/excelize/v2"
)

func Generate(dir, output string) {
	fmt.Println(dir, output)
	files, err := ioutil.ReadDir(dir)
	if err != nil {
		panic(err)
	}
	for _, file := range files {
		f, err := excelize.OpenFile(fmt.Sprintf("%s/%s", dir, file.Name()))
		if err != nil {
			fmt.Println(err)
			continue
		}

		sheet := f.GetSheetList()
		// Get all the rows in the Sheet1.
		rows, err := f.GetRows(sheet[0])
		if err != nil {
			fmt.Println(err)
			continue
		}

		head := "package config\n\nimport (\n\t\"fmt\"\n)"
		content := fmt.Sprintf("%s\n\n%s\n\n%s\n\n%s\n\n%s\n", head, GenStruct(rows), GenData(rows), GenFunc(rows), GenDefine(rows))
		filename := fmt.Sprintf("%s/%s.go", output, rows[0][0])
		fmt.Println(ioutil.WriteFile(filename, []byte(content), 0666))
		f.Close()
	}
}

func GenVariableName(str string) string {
	temp := strings.Split(str, "_")
	var upperStr string
	for y := 0; y < len(temp); y++ {
		vv := []rune(temp[y])
		for i := 0; i < len(vv); i++ {
			if i == 0 && vv[i] >= 97 && vv[i] <= 122 {
				vv[i] -= 32
			}
			upperStr += string(vv[i])
		}
	}
	return upperStr
}

func GenStruct(rows [][]string) string {
	fields := ""
	name := GenVariableName(rows[0][0])
	for k, cell := range rows[2] {
		if rows[4][k] != "define" && strings.Contains(cell, "s") {
			fields += fmt.Sprintf("\t%s %s\n", GenVariableName(rows[3][k]), rows[4][k])
		}
	}
	return fmt.Sprintf("type %s struct {\n%s}", name, fields)
}

func GenFunc(rows [][]string) string {
	name := GenVariableName(rows[0][0])
	format := "%d"
	if rows[4][0] == "string" {
		format = "%s"
	}
	return fmt.Sprintf("func Get%s(key %s) (cfg %s, err error) {\n\tvar ok bool\n\tif cfg, ok = %sData[key]; ok {\n\t\treturn\n\t}\n\terr = fmt.Errorf(\"%s Not Found Key:%s\", key)\n\treturn\n}", name, rows[4][0], name, name, name, format)
}

func GenData(rows [][]string) string {
	data := ""
	name := GenVariableName(rows[0][0])
	for _, line := range rows[5:] {
		fields := ""
		key := ""
		for k, cell := range rows[2] {
			if rows[4][k] != "define" && strings.Contains(cell, "s") {
				format := "\t\t%s: %s,\n"
				if rows[4][k] == "string" {
					format = "\t\t%s: \"%s\",\n"
				}
				if k == 0 {
					key = line[k]
				}
				fields += fmt.Sprintf(format, GenVariableName(rows[3][k]), line[k])
			}
		}
		if rows[4][0] == "string" {
			data += fmt.Sprintf("\t\"%s\": {\n%s\t},\n", key, fields)
			continue
		}
		data += fmt.Sprintf("\t%s: {\n%s\t},\n", key, fields)
	}
	return fmt.Sprintf("var %sData = map[%s]%s{\n%s\n}", name, rows[4][0], name, data)
}

func GenDefine(rows [][]string) string {
	data := ""
	for _, line := range rows[5:] {
		for k, _ := range rows[2] {
			if rows[4][k] == "define" {
				data += fmt.Sprintf("const %s = %s\n", GenVariableName(line[k]), line[0])
			}
		}
	}
	return data
}
