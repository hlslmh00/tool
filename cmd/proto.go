/*
Copyright © 2022 NAME HERE <EMAIL ADDRESS>

*/
package cmd

import (
	"github.com/spf13/cobra"

	"gitee.com/hlslmh00/tool/pkg/proto"
)

var protoDir string
var protoOut string
var protoPkg string

// protoCmd represents the proto command
var protoCmd = &cobra.Command{
	Use:   "proto",
	Short: "generate proto handler go file",
	Long:  `generate proto handler go file`,
	Run: func(cmd *cobra.Command, args []string) {
		proto.Generate(protoDir, protoOut, protoPkg)
	},
}

func init() {
	rootCmd.AddCommand(protoCmd)

	// Here you will define your flags and configuration settings.

	// Cobra supports Persistent Flags which will work for this command
	// and all subcommands, e.g.:
	// protoCmd.PersistentFlags().String("foo", "", "A help for foo")

	// Cobra supports local flags which will only run when this command
	// is called directly, e.g.:
	// protoCmd.Flags().BoolP("toggle", "t", false, "Help message for toggle")
	flags := protoCmd.Flags()
	flags.StringVar(&protoDir, "f", "test/proto", "proto dir")
	flags.StringVar(&protoOut, "o", "test/proto_out/router.go", "output file")
	flags.StringVar(&protoPkg, "p", "gitee.com/hlslmh00/game/pkg/pb", "proto pkg")

}
