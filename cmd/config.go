/*
Copyright © 2022 NAME HERE <EMAIL ADDRESS>

*/
package cmd

import (
	"github.com/spf13/cobra"

	"gitee.com/hlslmh00/tool/pkg/config"
)

var configDir string
var configOut string

// configCmd represents the config command
var configCmd = &cobra.Command{
	Use:   "config",
	Short: "convert config excel file to go file",
	Long:  `convert config excel file to go file`,
	Run: func(cmd *cobra.Command, args []string) {
		config.Generate(configDir, configOut)
	},
}

func init() {
	rootCmd.AddCommand(configCmd)

	// Here you will define your flags and configuration settings.

	// Cobra supports Persistent Flags which will work for this command
	// and all subcommands, e.g.:
	// configCmd.PersistentFlags().String("foo", "", "A help for foo")

	// Cobra supports local flags which will only run when this command
	// is called directly, e.g.:
	// configCmd.Flags().BoolP("toggle", "t", false, "Help message for toggle")
	flags := configCmd.Flags()
	flags.StringVar(&configDir, "f", "test/config", "excel dir")
	flags.StringVar(&configOut, "o", "test/config_out", "output dir")
}
